import React, { Component } from 'react';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import './Estilos/styles.css';

//Importar JS
import Consres from "./Consres.js"
import Constrab from "./Constrab.js"
import VerFacturas from "./VerFacturas.js"
import VerPresupuestos from "./VerPresupuestos.js"
import Encabezado from "./Encabezado.js";
import PiePagina from "./PiePagina.js"
import Login from "./Login.js"


//Iconos
import IconoRecibo from "./Imagenes/IconoRecibo.svg";
import IconoFactura from "./Imagenes/IconoFactura.png"
import IconoPresupuesto from "./Imagenes/IconoPresupuesto.png"
import IconoOT from "./Imagenes/IconoOT.png"




class MainMenu extends Component {
  render() {
    if (!this.props.match.params.token)
    {
        return <Login />
    }else{
      return (
            <div className="MainMenu">
            <Encabezado token={this.props.match.params.token} />
              <Tabs defaultTab="vertical-tab-one" vertical>
              <TabList>
                  <Tab tabFor="vertical-tab-one"><img src={IconoRecibo} size="10" alt="recibo" align="left" />Recibos</Tab>
                  <Tab tabFor="vertical-tab-two"><img src={IconoOT} alt="orden de trabajo" align="left" />Ordenes de Trabajo</Tab>
                  <Tab tabFor="vertical-tab-three"><img src={IconoFactura} alt="factura" align="left" />Facturas</Tab>
                    <Tab tabFor="vertical-tab-four"><img src={IconoPresupuesto} alt="presupuesto" align="left" />Presupuestos</Tab>
                </TabList>
                <TabPanel tabId="vertical-tab-one">
                  <Consres token={this.props.match.params.token}/>
                </TabPanel>
                <TabPanel tabId="vertical-tab-two">
                  <Constrab token={this.props.match.params.token} />
                </TabPanel>
                <TabPanel tabId="vertical-tab-three">
                  <VerFacturas token={this.props.match.params.token} />
                </TabPanel>
                <TabPanel tabId="vertical-tab-four">
                  <VerPresupuestos token={this.props.match.params.token} />
                </TabPanel>
              </Tabs>
              <PiePagina />
            </div>

        )
    }

  }
}



export default  MainMenu;
