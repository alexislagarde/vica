import React, { Component } from 'react';


//import de utilidades CSS
import './templatePrueba/css/demo.css';
import './templatePrueba/css/normalize.css';
import './templatePrueba/css/tabs.css';
import './templatePrueba/css/tabstyles.css';

import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';

//Importar JS
import Consres from "./Consres.js"
import Constrab from "./Constrab.js"
import VerFacturas from "./VerFacturas.js"
import VerPresupuestos from "./VerPresupuestos.js"
import Encabezado from "./Encabezado.js";
//import PiePagina from "./PiePagina.js"
import Login from "./Login.js"


//Imports de Iconos
import IconoRecibo from "./Imagenes/IconoRecibo.svg";
import IconoFactura from "./Imagenes/IconoFactura.png"
import IconoPresupuesto from "./Imagenes/IconoPresupuesto.png"
import IconoOT from "./Imagenes/IconoOT.png"



class MainMenu2 extends Component {
    render() {
        if (!this.props.match.params.token)
        {
            return <Login />
        }else{
        
        return (
            <body>
                <svg className="hidden">
                    <defs>
                        <path id="tabshape" d="M80,60C34,53.5,64.417,0,0,0v60H80z" />
                    </defs>
                </svg>
                <div className="container">
                    <div className="HeaderDerecho" align="left">
                            <header className="codrops-header">
                                <h1>Vica Neumaticos</h1>
                           </header>
                   </div> 
                   <div className="HeaderIzq" align="right">
                         <Encabezado />
                   </div>
                  
                    <div className="tabs tabs-style-underline">
                        
                        <Tabs defaultTab="one"className="tabs" >
                      <TabList>
                            <nav>
                                <ul>
                                      <li><Tab tabFor="one" className="TabSolo"><img className="icono" src={IconoRecibo} alt="recibo" align="center" />Recibos</Tab></li>
                                      <li><Tab tabFor="two" className="TabSolo"><img className="icono" src={IconoOT} alt="orden de trabajo" align="center" />Trabajo</Tab></li>
                                      <li><Tab tabFor="three" className="TabSolo"><img className="icono" src={IconoFactura} alt="factura" align="center" />Facturas</Tab></li>
                                      <li><Tab tabFor="four" className="TabSolo"><img className="icono" src={IconoPresupuesto} alt="presupuesto" align="center" />Presupuestos</Tab></li>
                               </ul>
                            </nav>    
                        </TabList>
                                <TabPanel tabId="one">
                                <Consres token={this.props.match.params.token}/>
                                </TabPanel>
                                <TabPanel tabId="two">
                                <Constrab token={this.props.match.params.token} />
                                </TabPanel>
                                <TabPanel tabId="three">
                                <VerFacturas token={this.props.match.params.token} />
                                </TabPanel>
                                <TabPanel tabId="four">
                                <VerPresupuestos token={this.props.match.params.token} />
                                </TabPanel>
                      </Tabs>
                      
                    </div>
		
		</div>
     

	</body >

           )
    }
}
}

export default MainMenu2


