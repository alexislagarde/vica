import React from 'react';
import ReactDOM from 'react-dom';
import {  Route, Switch, MemoryRouter } from 'react-router-dom';


//Estilo
import './App.css';

//Otros Sitios
import App from "./Login.js"
import Consres from "./Consres.js"
import Constrab from "./Constrab.js"
import VerFacturas from "./VerFacturas.js"
import VerPresupuestos from "./VerPresupuestos.js"
import MainMenu from "./MainMenu.js"
import NotFound from "./NotFound.js"
import MainMenu2 from "./MainMenu2.js"


//Servicio
import * as serviceWorker from './serviceWorker';




const App2 =  document.getElementById('root')
ReactDOM.render(
                    <MemoryRouter >
                      <Switch>
                        <Route exact path='/' component={App}/>
                        <Route exact path='/MainMenu/:token' component={MainMenu}/>
                        <Route path='/Consres' component={Consres}/>
                        <Route path='/Constrab' component={Constrab}/>
                        <Route path='/VerFacturas' component={VerFacturas}/>
                        <Route path='/VerPresupuestos' component={VerPresupuestos}/>
                        <Route exact path='/MainMenu2/:token' component={MainMenu2}/>
                        <Route component={NotFound}/>
                      </Switch>
                  </MemoryRouter >
                  

, App2);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//// 
serviceWorker.unregister();
