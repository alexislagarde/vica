import React, { Component } from 'react';
//import de libreria para manejar fechas
import Moment from 'react-moment';
//import  de hoja de estilos
import './Estilos/Consres.css';
//import de imagenes
import descarga from './Imagenes/download.png';
//Import para renderizar loading
import { trackPromise } from "react-promise-tracker";
import LoadingIndicator from "./LoadingIndicator.js";
//import de la URL de la api
import api from "./config/config.js"


class Consres extends Component {
  constructor() {
    super();
    this.state = {
      fecha_desde: '',
      fecha_hasta: '',
      items: [],
      detalle: [],
      err: '',
      recibosDistintos: [],
      selected: [],
      mostrarDetalle : ''
    }

    this.cambioFechaDesde = this.cambioFechaDesde.bind(this);
    this.cambioFechaHasta = this.cambioFechaHasta.bind(this);
    

  }

  cambioFechaHasta(e) {
    this.setState({
      fecha_hasta: e.target.value
    })
  }


  cambioFechaDesde(e) {
    this.setState({
      fecha_desde: e.target.value
    })
  }


  handleSubmit = async e => {
    e.preventDefault()
    if (this.state.fecha_desde === '') {
      this.setState({ err: "***Seleccione el campo Fecha Desde***" })
    } else if (this.state.fecha_hasta === '') {
      this.setState({ err: "***Seleccione el campo Fecha Hasta ***" })
    } else {
      var urlencoded = new URLSearchParams();
      urlencoded.append("fecha_desde", this.state.fecha_desde);
      urlencoded.append("fecha_hasta", this.state.fecha_hasta);
      try {
        let config = {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this.props.token
          },
          body: urlencoded
        }
        let res = await  trackPromise(fetch(`${api}recibos/consulta`, config))
        let json = await res.json()
        if (json.recibos.length === 0) {
          this.setState({ err: "***No hay recibos entre esas fechas***" })
        } else {

          this.setState({
            items: json.recibos
          })
         
          this.ordenarRecibos(json.recibos)
          this.setState({ err: "" })


          return json;
        }
      } catch (error) {
        console.log(error)

      }
    }
  }

  ordenarRecibos(recibos) {
    let recibosOrdenados = recibos.sort()
    this.setState({ items: recibosOrdenados })
    
    this.separarRecibos();
  }

  separarRecibos() {
    let recibosUnicos = []
    let index = this.state.items.length - 1
    let id_inicial = this.state.items[index].recibo_id
    for (var i = 0; i < this.state.items.length; i++) {
      if (this.state.items[i].recibo_id !== id_inicial) {
        recibosUnicos[i] = this.state.items[i]
        id_inicial = recibosUnicos[i].recibo_id
      }else if(id_inicial === this.state.items[this.state.items.length-1].recibo_id) {
        recibosUnicos[0] = this.state.items[0]
      } 
      if (this.state.items.length === 1) {
        recibosUnicos = this.state.items
      }
    }
    this.setState({ recibosDistintos: recibosUnicos })
   
    return recibosUnicos;
  }

  render() {
    
    return (
      <div className="Consres">
        <div className="Conres-body">
          <form onSubmit={this.handleSubmit} >
            <table className="Conres-table">
              <tr>
                <td>  Desde:  </td>
                <td > <input id="fecha_desde" type="date" name="fecha_desde" placeholder="algo" onChange={this.cambioFechaDesde} /></td>
                <td >  Hasta: </td>
                <td > <input id="fecha_hasta" type="date" name="fecha_hasta" placeholder="algo" onChange={this.cambioFechaHasta} /></td>
                <td><button className="BotonConsultar" onSubmit={this.handleSubmit}> <img src="./Imagenes/Search.png" alt="" /> </button></td>
                <td><label className="labelErr" align="center">{this.state.err}</label></td>
              </tr>
              
            </table>
          </form >
          <LoadingIndicator />
          <label align="center" className="NombreDoc">Recibos</label>          
          <div className="divDeTabla" >
           <table className="tableResponsive">
              <thead>
                <tr>
                  <th>Nro. de recibo</th>
                  <th>Fecha y hora</th>
                  <th>Total</th>
                  <th>Usuario</th>
                  <th>Cobrador</th>
                  <th>Sucursal</th>
                  <th>Observacion</th>
               
                </tr>
              </thead>
              <tbody className="bodyResponsive">
                {this.state.recibosDistintos !== [] ?
                  this.state.recibosDistintos.map((item, i) =>
                    <tr
                      onClick={() => {

                        let reciboDetalle = []

                        for (var i = 0; i < this.state.items.length; i++) {
                          if (this.state.items[i].recibo_id === item.recibo_id) {
                            reciboDetalle[i] = this.state.items[i]
                            this.state.selected.push(reciboDetalle[i])

                          } if (this.state.items.length === 1) {
                            reciboDetalle = this.state.items
                          }
                        }

                        this.setState({ selected: reciboDetalle})
                         this.setState( {mostrar : this.state.selected[this.state.selected.length -1].nro_recibo})
                        console.log(this.state.mostrar)

                      }

                      }>
                      <td align="left">{item.nro_recibo}</td>
                      <td align="left"><Moment format="DD/MM/YYYY HH:mm">{item.fechahora}</Moment></td>
                      <td align="left">$ {item.pago_importe}</td>
                      <td align="left">{item.usuario}</td>
                      <td align="left">{item.cobrador}</td>
                      <td align="left">{item.sucursal_nombre}</td>
                      <td align="left">{item.observaciones === null ? 'Sin Observaciones' : item.observaciones}</td>
                      
                    </tr>
                  )
                  : null
                }
                 

              </tbody>
            </table>
            
          </div>
              <div className="NombreDoc2"> <label align="left" >Items del Recibo : {this.state.mostrar}</label></div>
          <div className="divDeTabla2">
            <table className="tableResponsive2">
              <thead>
                <tr>
                  <th>Descripcion</th>
                  <th>Importe</th>
                </tr>
              </thead>
              <tbody className="bodyResponsive2">
                {this.state.selected.length > 0 ?
                  this.state.selected.map((item, i) =>
                    <tr>
                      <td>{item.recibo_linea_descripcion}</td>
                      <td>{item.recibo_linea_importe}</td>
                    </tr>
                  )
                                                                                  
                  : null
                }

              </tbody>
            </table>
          </div>
          <div className="NombreDoc3"> <label align="left" >Pago del Recibo : {this.state.mostrar}</label></div>
          <div className="divDeTabla3">
            <table className="tableResponsive2">
              <thead>
                <tr>
                  <th>Forma de Pago</th>
                  <th>Importe</th>

                </tr>
              </thead>
              <tbody className="bodyResponsive2">
                {
                  this.state.selected.length>0 ?
                  <tr>
                      <td>{this.state.selected[this.state.selected.length-1].pago_nombre}</td>
                      <td>{this.state.selected[this.state.selected.length-1].pago_importe}</td>
                  </tr>
                : null
                }

              </tbody>
            </table>
          </div>

        </div>
      </div>
    );
  }
};


export default Consres;

