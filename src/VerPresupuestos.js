import React, { Component } from 'react';
//import hoja de estilo
import './Estilos/Consres.css';
//Import para renderizar loading
import { trackPromise } from "react-promise-tracker";
import LoadingIndicator from "./LoadingIndicator.js";
//import de la URL de la api
import api from "./config/config.js"
//import libreria para manejar formato fecha
import Moment from 'react-moment';
//import icono de descarga
import descarga from './Imagenes/download.png';


class VerPresupuestos extends Component {
  constructor() {
    super();
    this.state = {
      fecha_desde: '',
      fecha_hasta: '',
      items: [],
      detalle: null,
      err: "",
      presupuestosDistintos: [],
      selected: []
    }

    this.cambioFechaDesde = this.cambioFechaDesde.bind(this);
    this.cambioFechaHasta = this.cambioFechaHasta.bind(this);
    // this.getItems = this.getItems.bind(this);

  }//cierra el constructor


  cambioFechaHasta(e) {
    this.setState({
      fecha_hasta: e.target.value
    })
  }


  cambioFechaDesde(e) {
    this.setState({
      fecha_desde: e.target.value
    })
  }

  handleSubmit = async e => {
    e.preventDefault()
    if (this.state.fecha_desde === '') {
      this.setState({ err: "***Seleccione el campo Fecha Desde***" })
    } else if (this.state.fecha_hasta === '') {
      this.setState({ err: "***Seleccione el campo Fecha Hasta***" })
    } else {
      var urlencoded = new URLSearchParams();
      urlencoded.append("fecha_desde", this.state.fecha_desde);
      urlencoded.append("fecha_hasta", this.state.fecha_hasta);
      // urlencoded.append("fecha_desde", "2016-03-02");
      // urlencoded.append("fecha_hasta", "2020-04-04");
      try {
        let config = {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this.props.token

          },
          body: urlencoded
        }
        let res = await  trackPromise(fetch(`${api}presupuestos/consulta`, config))
        let json = await res.json()
        if (json.presupuestos.length === 0) {
          this.setState({ err: "No hay presupuestos en esas fechas" })
        } else {
          this.setState({
            items: json.presupuestos
          })
          console.log("pres sin ordenar")
          console.log(json.presupuestos)
          this.ordenarPresupuestos(json.presupuestos)
          this.setState({ err: "" })
          return json;
        }
      } catch (error) {
        console.log(error)

      }
    }
  }
  ordenarPresupuestos(presupuestos) {
    let presupuestosOrdenados = presupuestos.sort()
    this.setState({ items: presupuestosOrdenados })
    console.log("trabajos ordenados")
    console.log(this.state.items)
    this.separarTrabajos();
  }

  separarTrabajos() {
    let presupuestosUnicos = []
    let index = this.state.items.length - 1
    let id_inicial = this.state.items[index].presupuesto_id
    for (var i = 0; i < this.state.items.length; i++) {
      if (this.state.items[i].presupuesto_id !== id_inicial) {
        presupuestosUnicos[i] = this.state.items[i]
        id_inicial = presupuestosUnicos[i].presupuesto_id
      } else if (this.state.items[0].presupuesto_id === this.state.items[this.state.items.length - 1].presupuesto_id) {
        presupuestosUnicos[0] = this.state.items[0]
      }
      else if (this.state.items.length === 1) {
        presupuestosUnicos = this.state.items
      }
    }
    this.setState({ presupuestosDistintos: presupuestosUnicos })
    console.log("presupuestos distintos")
    console.log(this.state.presupuestosDistintos)
    return presupuestosUnicos;
  }




  render() {

    return (
      <div className="Consres">
        <div className="Conres-body">
          <form onSubmit={this.handleSubmit} >
            <table className="Conres-table">
              <tr>
                <td>  Desde:  </td>
                <td > <input id="fecha_desde" type="date" name="fecha_desde" placeholder="algo" onChange={this.cambioFechaDesde} /></td>
                <td >  Hasta: </td>
                <td > <input id="fecha_hasta" type="date" name="fecha_hasta" placeholder="algo" onChange={this.cambioFechaHasta} /></td>
                <td><button className="BotonConsultar" onSubmit={this.handleSubmit}></button></td>
                <td><label className="labelErr" align="center">{this.state.err}</label></td>
              </tr>
            </table>
          </form >
          <LoadingIndicator />
          <label align="center" className="NombreDoc">Prespuestos</label>     
          <div className="divDeTabla" >
            <table className="tableResponsive">
              <thead>
                <tr>
                  <th align="left">Fecha</th>
                  <th align="left">Numero</th>
                  <th align="left">Vehiculo</th>
                  <th align="left">Total</th>
                  <th align="left">Vendedor</th>
                  <th align="left">Orden de trabajo</th>
                  <th align="left">Forma de pago</th>
                  <th align="left">Sucursal</th>
                  <th align="left">Observacion</th>
                  <th align="left">Situaciones</th>
                  
                </tr>
              </thead>
              <tbody className="bodyResponsive">
                {this.state.presupuestosDistintos !== [] ?
                  this.state.presupuestosDistintos.map((item, i) =>
                    <tr
                      onClick={() => {

                        let presupuestosDetalle = []

                        for (var i = 0; i < this.state.items.length; i++) {
                          if (this.state.items[i].orden_id === item.orden_id) {
                            presupuestosDetalle[i] = this.state.items[i]
                            this.state.selected.push(presupuestosDetalle[i])

                          } if (this.state.items.length === 1) {
                            presupuestosDetalle = this.state.items
                          }
                        }

                        this.setState({ selected: presupuestosDetalle })


                      }

                      }>

                      <td align="left"><Moment format="DD/MM/YYYY HH:mm">{item.fechahora}</Moment></td>
                      <td align="left">{item.nro_presupuesto}</td>
                      <td align="left">{item.vehiculo}</td>
                      <td align="left">TOTAL</td>
                      <td align="left">{item.vendedor}</td>
                      <td align="left">Orden de trabajo</td>
                      <td align="left">{item.pago_nombre}</td>
                      <td align="left">{item.sucursal_nombre}</td>
                      <td align="left">Observacion</td>
                      <td align="left">situaciones</td>
                     

                    </tr>
                    )
                    : null
                  }
              </tbody>
            </table>                                              
        </div>
        <label align="center" className="NombreDoc2">Detalle del Prespuesto</label> 
        <div className="divDeTabla4">
            <table className="tableResponsive2">
              <thead>
                <tr>
                  <th align="left">Codigo </th>
                  <th align="left">Descripcion</th>
                  <th align="center">P.U c/IVA</th>
                  <th align="center">Desc/Rec</th>
                  <th align="center">Subtotal</th>
                  </tr>
              </thead>
              <tbody className="bodyResponsive2">
              {this.state.selected.length > 0 ?
                  this.state.selected.map((item, i) =>
                    <tr>
                      <td>{item.presupuesto_linea_id}</td>
                      <td>{item.descripcion}</td>
                      <td>falta PU</td>
                      <td>{item.promocion_linea_id}</td>
                      <td>subtotal</td>
                    </tr>
                  )
                                                                                  
                  : null
                }
              </tbody>
            </table>
        
        </div>
        <div>
          

        </div>



      </div>
      </div >
    );
  }
};


export default VerPresupuestos;
