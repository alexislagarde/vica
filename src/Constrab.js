import React, { Component } from 'react';
//Import hojas de estilo
import './Estilos/Consres.css';
//Import libreria para manejar formato fecha
import Moment from 'react-moment';
//Import para renderizar loading
import { trackPromise } from "react-promise-tracker";
import LoadingIndicator from "./LoadingIndicator.js";

//import de la URL de la api
import api from "./config/config.js"
import descarga from './Imagenes/download.png';

class Constrab extends Component {
  constructor() {
    super();
    this.state = {
      fecha_desde: '',
      fecha_hasta: '',
      items: [],
      detalle: null,
      err: '',
      trabajosDistintos: [],
      selected: []
    }

    this.cambioFechaDesde = this.cambioFechaDesde.bind(this);
    this.cambioFechaHasta = this.cambioFechaHasta.bind(this);
    // this.getItems = this.getItems.bind(this);

  }//cierra el constructor

  cambioFechaHasta(e) {
    this.setState({
      fecha_hasta: e.target.value
    })
  }

  cambioFechaDesde(e) {
    this.setState({
      fecha_desde: e.target.value
    })
  }
 
  handleSubmit = async e => 
  {
    e.preventDefault()
    if (this.state.fecha_desde === '') {
      this.setState({ err: "***Seleccione el campo Fecha Desde***" })
    } else if (this.state.fecha_hasta === '') {
      this.setState({ err: "***Seleccione el campo Fecha Hasta ***" })
    } else {
      var urlencoded = new URLSearchParams();
      urlencoded.append("fecha_desde", this.state.fecha_desde);
      urlencoded.append("fecha_hasta", this.state.fecha_hasta);
      // urlencoded.append("fecha_desde", "2016-03-02");
      // urlencoded.append("fecha_hasta", "2020-04-04");
      try {
        
        let config = {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this.props.token

          },
          body: urlencoded
        }
       
        let res =  await trackPromise( fetch(`${api}trabajos/consulta`, config))
        let json = await res.json()
      
        
        if (json.trabajos.length === 0) {
          this.setState({ err: "***No hay ordenes de trabajo entre esas fechas***" })
        } else {
          this.setState({
            items: json.trabajos
          })
         
          this.ordenarTrabajos(json.trabajos)
          this.setState({ err: "" })
          return json;
        }
      }catch (error) {
        console.log(error)

     }
  }
}
    
    ordenarTrabajos(trabajos) 
    {
      let trabajosOrdenados = trabajos.sort()
      this.setState({ items: trabajosOrdenados })
      console.log("trabajos ordenados")
      console.log(this.state.items)
      this.separarTrabajos();
    }
    separarTrabajos() 
    {
      let trabajosUnicos = []
      let index = this.state.items.length - 1
      let id_inicial = this.state.items[index].orden_id
      for (var i = 0; i < this.state.items.length; i++) {
        if (this.state.items[i].orden_id !== id_inicial) {
          trabajosUnicos[i] = this.state.items[i]
          id_inicial = trabajosUnicos[i].orden_id
        }else if(id_inicial === this.state.items[this.state.items.length-1].orden_id) {
          trabajosUnicos[0] = this.state.items[0]
        }
        else if (this.state.items.length === 1) {
          trabajosUnicos = this.state.items
        }
      }
      this.setState({ trabajosDistintos: trabajosUnicos })
      console.log("trabajos distintos")
      console.log(this.state.trabajosDistintos)
      return trabajosUnicos;
    }

    
  render() {

    return (
      <div className="Consres">
        <div className="Conres-body">
          <form onSubmit={this.handleSubmit} >
            <table className="Conres-table">
              <tr>
                <td>  Desde:  </td>
                <td > <input id="fecha_desde" type="date" name="fecha_desde" placeholder="algo" onChange={this.cambioFechaDesde} /></td>
                <td >  Hasta: </td>
                <td > <input id="fecha_hasta" type="date" name="fecha_hasta" placeholder="algo" onChange={this.cambioFechaHasta} /></td>
                <td><button className="BotonConsultar" onSubmit={this.handleSubmit} > </button></td>
                <td><label className="labelErr" align="center">{this.state.err}</label></td>
              </tr>
            </table>
          </form >
          <LoadingIndicator />
         <label align="center" className="NombreDoc">Ordenes de Trabajo</label>     
            <div className="divDeTabla" >
            <table className="tableResponsive">
              <thead>
                  <tr>
                      <th align="left">Estado</th>
                      <th align="left">Fecha y Hora</th>
                      <th align="left">Compromiso</th>
                      <th align="left">Numero</th>
                      <th align="left">Sucursal</th>
                      <th align="left">Vehiculo</th>
                      <th align="left">Total</th>
                      <th align="left">Factura</th>
                      <th align="left">Observacion</th>
                     
                    </tr>
                </thead>
                <tbody className="bodyResponsive">
                  {this.state.trabajosDistintos !== [] ?
                    this.state.trabajosDistintos.map((item, i) =>
                    <tr
                    onClick={() => {

                      let trabajoDetalle = []

                      for (var i = 0; i < this.state.items.length; i++) {
                        if (this.state.items[i].orden_id === item.orden_id) {
                          trabajoDetalle[i] = this.state.items[i]
                          this.state.selected.push(trabajoDetalle[i])

                        } if (this.state.items.length === 1) {
                          trabajoDetalle = this.state.items
                        }
                      }

                      this.setState({ selected: trabajoDetalle})


                    }

                    }>
                        <td align="left">{item.estado}</td>
                        <td align="left"><Moment format="DD/MM/YYYY HH:mm">{item.fecha_hora}</Moment></td>
                        <td align="left"><Moment format="DD/MM/YYYY">{item.fecha_compromiso}</Moment></td>
                        <td align="left">{item.nro_orden}</td>
                        <td align="left">{item.nombre}</td>
                        <td align="left">{item.chapa}</td>
                        <td align="left">Total</td>
                        <td align="left">aca debe ir factura</td>
                        <td align="left">{item.producto_descripcion}</td>
                        
                      </tr>
                    )
                    : null
                  }
              </tbody>
            </table>
          </div>
         
          <label align="center" className="NombreDoc">Detalle de la orden</label>
          <div className="divDeTabla4">
            <table className="tableResponsive">
              <thead>
                <tr>
                    <th align="left">Codigo </th>
                    <th align="left">Descripcion</th>
                    <th align="left">Presupuesto</th>
                    <th align="left">Responsable</th>
                    <th align="left">P.U. c/IVA</th>
                    <th align="left">Desc/Rec</th>
                    <th align="left">Subtotal</th>
                  </tr>
                </thead>
                    <tbody className="bodyResponsive"> 
                    {this.state.selected.length > 0 ?
                  this.state.selected.map((item, i) =>
                  <tr>
                        <td align="left">{item.nro_trabajo}</td>
                        <td align="center">{item.orden_producto_stk_descripcion}</td>
                        <td align="right">Presupuesto</td>
                        <td align="right">{item.orden_trabajo_responsable}</td>
                        <td align="right">{item.orden_producto_stk_precio}</td>
                        <td align="right">{item.orden_producto_stk_bonificacion}</td>
                        <td align="right">{item.orden_producto_stk_precio}</td>
                        
                  </tr>
                  )
                                                                                  
                  : null

                }
             </tbody>
            </table>
          </div>

        </div>
      </div>
    );
  }
};


export default Constrab;

