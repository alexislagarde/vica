import React, { Component } from 'react';

//Import hoja de estilos de la pagina
import './Estilos/Consres.css';

//Import libreria de conversion de fechas
import Moment from 'react-moment';
//Import para renderizar loading
import { trackPromise } from "react-promise-tracker";
import LoadingIndicator from "./LoadingIndicator.js";
//import de la URL de la api
import api from "./config/config.js"
//import icono
import descarga from './Imagenes/download.png';
import base64 from 'base64topdf';

class VerFacturas extends Component {
  constructor() {
    super();
    this.state = {
      fecha_desde: '',
      fecha_hasta: '',
      items: [],
      err: '',
      detalle: [],
      facturasDistintas: [],
      selected: [],
      mostrar: ''
    }

    this.cambioFechaDesde = this.cambioFechaDesde.bind(this);
    this.cambioFechaHasta = this.cambioFechaHasta.bind(this);
    // this.getItems = this.getItems.bind(this);

  }//cierra el constructor

  cambioFechaHasta(e) {
    this.setState({
      fecha_hasta: e.target.value
    })
  }


  cambioFechaDesde(e) {
    this.setState({
      fecha_desde: e.target.value
    })
  }



  handleSubmit = async e => {
    e.preventDefault()
    if (this.state.fecha_desde === '') {
      this.setState({ err: "***Seleccione el campo Fecha Desde***" })
    } else if (this.state.fecha_hasta === '') {
      this.setState({ err: "***Seleccione el campo Fecha Hasta***" })
    } else {
      var urlencoded = new URLSearchParams();
      urlencoded.append("fecha_desde", this.state.fecha_desde);
      urlencoded.append("fecha_hasta", this.state.fecha_hasta);
      // urlencoded.append("fecha_desde", "2016-03-02");
      // urlencoded.append("fecha_hasta", "2020-04-04");
      try {
        let config = {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this.props.token


          },
          body: urlencoded
        }
        let res = await trackPromise(fetch(`${api}facturas/consulta`, config))
        let json = await res.json()
        if (json.facturas.length === 0) {
          this.setState({ err: "No hay facturas en esas fechas" })
        } else {
          this.setState({
            items: json.facturas
          })
          this.ordenarFacturas(json.facturas)
          this.setState({ err: "" })
          return json;
        }

      } catch (error) {
        console.log(error)

      }
    }
  }
  ordenarFacturas(facturas) {
    let facturasOrdenadas = facturas.sort()
    this.setState({ items: facturasOrdenadas })

    this.separarFacturas();
  }

  separarFacturas() {
    let facturasUnicas = []
    let index = this.state.items.length - 1
    let id_inicial = this.state.items[index].factura_id
    for (var i = 0; i < this.state.items.length; i++) {
      if (this.state.items[i].factura_id !== id_inicial) {
        facturasUnicas[i] = this.state.items[i]
        id_inicial = facturasUnicas[i].factura_id
      } else if (id_inicial === this.state.items[this.state.items.length - 1].factura_id) {
        facturasUnicas[0] = this.state.items[0]
      }
      if (this.state.items.length === 1) {
        facturasUnicas = this.state.items
      }
    }
    this.setState({ facturasDistintas: facturasUnicas })

    return facturasUnicas;
  }

  descargarPDF = () =>
  {
      if(this.state.mostrar!== '') 
      {
        this.setState({err:"*Futura Descarga**"})
      }else{ this.setState({err:"**No selecciono factura**"})
    }

 /*  try{
    let base64Str=""
    let decodePDF = base64.base64Decode('./pdf/prueba.pdf')
    console.log(decodePDF)
  }catch(error){console.log(error)}
 */

  }


  render() {
    return (
      <div className="Consres">
        <div className="Conres-body">
          <form onSubmit={this.handleSubmit} >
            <table className="Conres-table">
              <tr>
                <td>  Desde: </td>
                <td > <input id="fecha_desde" type="date" name="fecha_desde"  onChange={this.cambioFechaDesde} /></td>
                <td >  Hasta:</td>
                <td > <input id="fecha_hasta" type="date" name="fecha_hasta"  onChange={this.cambioFechaHasta} /></td>
                <td><button className="BotonConsultar" onSubmit={this.handleSubmit} ></button></td>
                <td><label className="labelErr" align="center">{this.state.err}</label></td>
              </tr>
            </table>
          </form >
          <LoadingIndicator />
          <label align="center" className="NombreDoc">Facturas {this.state.mostrar}</label>
         
          { this.state.selected !== [] ?
         
            <div className="NombreDoc2">
                   <button className="btnDescarga"  onClick={this.descargarPDF} title="Descargar PDF" alt="Descarga" value="PDF" />    
           </div>
            :null
                
          
           }
         
          
          <div className="divDeTabla" >
            <table className="tableResponsive">
              <thead>
                <tr>
                  <th align="left">Fecha</th>
                  <th align="left">Nro. de Factura</th>
                  <th align="left">Total</th>
                  <th align="center">Vendedor</th>
                  <th align="left">Sucursal</th>
                  <th align="left">Observacion</th>
                  <th align="left">O.E</th>
                  <th align="left">C.A.E</th>
                </tr>
              </thead>
              <tbody className="bodyResponsive">
                {this.state.facturasDistintas !== [] ?
                  this.state.facturasDistintas.map((item, i) =>
                    <tr
                      onClick={() => {

                        let facturaDetalle = []

                        for (var i = 0; i < this.state.items.length; i++) {
                          if (this.state.items[i].factura_id === item.factura_id) {
                            facturaDetalle[i] = this.state.items[i]
                            this.state.selected.push(facturaDetalle[i])

                          } if (this.state.items.length === 1) {
                            facturaDetalle = this.state.items
                          }
                        }

                        this.setState({ selected: facturaDetalle})
                         this.setState( {mostrar : this.state.selected[this.state.selected.length -1].nro_factura})
                       
                      }

                      }>
                          <td align="left"><Moment format="DD/MM/YYYY HH:mm">{item.fecha}</Moment></td>
                          <td align="left">{item.nro_factura}</td>
                          <td align="left">Total</td>
                          <td align="center">{item.vendedor}</td>
                          <td align="left">{item.sucursal_nombre}</td>
                          <td align="left">{item.observacion}</td>
                          <td align="left">oe</td>
                          <td align="left">{item.cae}</td>
                                   
                          
                      </tr>
                  )
                  : null
                } 
                

              </tbody>
            </table>
        </div>


      
      <div className="NombreDoc2"> <label align="left" >Items de la Factura : {this.state.mostrar}</label></div>
          <div className="divDeTabla2">
            <table className="tableResponsive2">
              <thead>
                <tr>
                  <th>Codigo</th>
                  <th>Descripcion</th>
                  <th>P.U .c/IVA</th>
                  <th>Desc/Rec</th>
                  <th>IVA</th>
                  <th>SubTotal</th>
                </tr>
              </thead>
              <tbody className="bodyResponsive2">
                {this.state.selected.length > 0 ?
                  this.state.selected.map((item, i) =>
                    <tr>
                      <td>{item.factura_linea_id}</td>
                      <td>{item.descripcion}</td>
                      <td>P.U .c/IVA</td>
                      <td>Desc/Rec</td>
                      <td>IVA</td>
                      <td>SubTotal</td>

                    </tr>
                  )
                                                                                  
                  : null
                }

              </tbody>
            </table>
          </div>
          <div className="NombreDoc3"> <label align="left" >Pago de la Factura : {this.state.mostrar}</label></div>
          <div className="divDeTabla3">
            <table className="tableResponsive2">
              <thead>
                <tr>
                  <th>Forma de Pago</th>
                  <th>Subtotal</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody className="bodyResponsive2">
                {
                  this.state.selected.length>0 ?
                  <tr>
                      <td>{this.state.selected[this.state.selected.length-1].pago_nombre}</td>
                      <td>SubTotal</td>
                      <td>Total</td>
                  </tr>
                : null
                }

              </tbody>
            </table>
          </div>

        </div>
        </div>
     
    );
  }
};


export default VerFacturas;
