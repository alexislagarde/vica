import React, {Component} from 'react';

import "./Estilos/CarouselPrueba.css"
//import { history } from "react-router-dom";
//import MainMenu from "./MainMenu.js"
import api from "./config/config.js"


class App extends Component {
  constructor() {
    super();

    this.state = {
                  usuario: '',
                  password:'',
                  items: [],
                  err: ''

                  }
                  this.cambioUsuario = this.cambioUsuario.bind(this);
                  this.cambioPassword = this.cambioPassword.bind(this);
                  //this.cambioItems = this.cambioItems.bind(this);
                }


                                  cambioUsuario(e) {
                                    this.setState( {
                                      usuario: e.target.value
                                    })
                                  }
                                 cambioPassword(e)
                                  {
                                    this.setState( {
                                        password: e.target.value
                                    })
                                  }

                                    redirect(p_token)
                                    {
                                      let token = p_token
                                      if(p_token !== '')
                                      {this.props.history.replace(`/MainMenu2/${token}`)

                                         // this.props.history.replace(`/MainMenu/${token}`)
                                      }else{  this.props.history.push("/")}
                                    }

                                         handleSubmit = async e => {
                                           e.preventDefault();
                                           if (this.state.usuario === '')
                                           {
                                            this.setState({err:"***Campo Usuario Vacio***"})
                                           }else if (this.state.password === '')
                                           {
                                               this.setState({err:"***Campo Password Vacio***"})
                                           }else {
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

                                                    var urlencoded = new URLSearchParams();
                                                    urlencoded.append("usuario", this.state.usuario);
                                                    urlencoded.append("password", this.state.password);
                                                    console.log(urlencoded);
                                                    try{
                                                    var config = {
                                                      method: 'POST',
                                                      headers: myHeaders,
                                                      body: urlencoded,
                                                      redirect: 'follow'
                                                };

                                                let res = await  fetch(`${api}usuarios/login`, config)
                                                let json = await  res.json()
                                                        if (json.auth)
                                                        {
                                                          this.setState({
                                                            items: json
                                                          })
                                                          let p_token = this.state.items.token
                                                          this.redirect(p_token)
                                                        }
                                                        else{this.setState({err:"***USUARIO O PASSWORD INCORRECTO***"})}
                                            }
                                              catch(error){console.log(error)}}
                                            }


  render() {
    return (
<div className="SlideLogin">
                <div className="App-body">
                   <div>
                      <div className="limite">
                            <div className="container-login" id="container-login">
                                  <div className="App-login" id="App-login" >
                                        <form onSubmit={this.handleSubmit} className="login-form validate-form">
                                        <span className="login-form-logo">
                                        </span>
                                        <span className="login-form-title p-b-34 p-t-27">
                                          Iniciar Sesion
                                        </span>
                                        <div className="form-input">
                                               <label className="LabelErr" >{this.state.err}</label>
                                               <input  className="App-inputs"  type="text" name="Usuario" placeholder="Usuario" onChange = {this.cambioUsuario} />
                                               <span className="focus-input" data-placeholder="&#xf207;"></span>
                                               <input className="App-inputs" type="password" name="password" placeholder="Password" onChange = {this.cambioPassword} />
                                               <span className="focus-input" data-placeholder="&#xf191;"></span>
                                         </div>
                                         <div className="Container-App-button">

                                                <button  className="Boton-Login" value="Ingresar"
                                                    onSubmit ={this.handleSubmit}> Ingresar </button>


                                        </div>

                                       </form>

                                  </div>
                            </div>
                          </div>

                  </div>
</div>
</div>
);
}
}
export default App;
